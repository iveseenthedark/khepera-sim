import World from '../world/World';
import { StaticObstacle } from '../world/StaticObstacle';
import Light from '../world/Light';
import { architect, Network, Neat } from 'neataptic';
import NeatapticController from '../controllers/NeatapticController';
import Agent from '../world/Agent';
import PhotoSensor from '../world/PhotoSensor';
import Point from '../primitives/Point';
import SimMaths from '../maths/SimMaths';

const ctx: Worker = self as any;
const POPULATION_SIZE = 100;
const MUTATION_RATE = 0.3;
const ELITISM = Math.round(0.1 * POPULATION_SIZE);

const TRIALS = 20;
const TRIAL_LENGTH = 50;

let is_running = false;
let iteration = 0;

const nn_config = { A: 2, B: 3, C: 2 };

// Create world
const world = new World(640, 560);
const light = new Light({ x: 320, y: 280 });
world.add_element(...StaticObstacle.create_boundary_wall(world), light);

const agent = new Agent(
	{ x: 310, y: 150 },
	new NeatapticController(architect.LSTM(nn_config.A, nn_config.B, nn_config.C))
);

agent.add_sensor(new PhotoSensor(agent, Math.PI/4));
agent.add_sensor(new PhotoSensor(agent, -Math.PI/4));
world.add_element(agent);

const eval_fitness = (genome: Network) => {
	// Create agent
	const controller = new NeatapticController(genome);

	// Run trails
	let fitness = 0;
	for (let t = 0; t < TRIALS; t++) {
		// Move light to random location
		light.position = Point.random([50, world.width - 50], [50, world.height - 50]);
		// Move agent to random location
		agent.position = Point.random([50, world.width - 50], [50, world.height - 50]);

		// Start distance from light
		const distance_initial = SimMaths.distance(light.position, agent.position);
		let distance_min = distance_initial;
		for (let s = 0; s < TRIAL_LENGTH; s++) {
			world.update();
			distance_min = Math.min(SimMaths.distance(light.position, agent.position), distance_min);
		}
		fitness += 1 - distance_min / distance_initial;
	}
	return fitness / TRIALS;
};

const neat = new Neat(nn_config.A, nn_config.C, eval_fitness, {
	popsize: POPULATION_SIZE,
	mutationRate: MUTATION_RATE,
	elitism: ELITISM,
	network: architect.LSTM(nn_config.A, nn_config.B, nn_config.C)
});

const run = () => {
	neat.evolve().then((fittest: any) => {
		ctx.postMessage({ iteration: ++iteration, score: fittest.score, fittest: fittest.toJSON() });
		if (is_running) setTimeout(run, 1);
	});
};

ctx.addEventListener('message', (e: MessageEvent) => {
	const { cmd } = e.data;
	switch (cmd) {
		case 'start': {
			is_running = true;
			console.log('Evolution Started....');
			run();
			break;
		}
		case 'stop': {
			console.log('Evolution Stopped....');
			is_running = false;
			break;
		}
	}
});
