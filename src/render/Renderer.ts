import World from '../world/World';
import Body from '../core/Body';

/**
 */
export default class Renderer {
	private readonly ctx: CanvasRenderingContext2D;
	private is_running: boolean;
	private animation_frame_idx?: number;

	private constructor(private readonly canvas: HTMLCanvasElement, private readonly world: World) {
		this.ctx = <CanvasRenderingContext2D>canvas.getContext('2d');
		this.is_running = false;
	}

	/**
	 * Builder for a new Renderer
	 */
	static create(opts: { element: HTMLElement; world: World }): Renderer {
		// Create a canvas to draw on and add it to the element
		const canvas = document.createElement('canvas');

		// Add 1px as we translate the rendering 0.5 in both directions to avoid blurring
		canvas.width = opts.world.width + 1;
		canvas.height = opts.world.height + 1;

		opts.element.style.width = canvas.width + 'px';
		opts.element.style.height = canvas.height + 'px';
		opts.element.appendChild(canvas);

		return new Renderer(canvas, opts.world);
	}

	private fix_dpi() {
		//create a style object that returns width and height
		let style = {
			height: (): number =>
				Number.parseInt(
					getComputedStyle(this.canvas)
						.getPropertyValue('height')
						.slice(0, -2)
				),
			width: (): number =>
				Number.parseInt(
					getComputedStyle(this.canvas)
						.getPropertyValue('width')
						.slice(0, -2)
				)
		};
		//set the correct attributes for a crystal clear image!
		this.canvas.setAttribute('width', String(style.width() * window.devicePixelRatio));
		this.canvas.setAttribute('height', String(style.height() * window.devicePixelRatio));
		this.ctx.scale(window.devicePixelRatio, window.devicePixelRatio);
		this.ctx.translate(0.5, 0.5);
	}

	/**
	 * Start the rendering
	 */
	start() {
		this.is_running = true;
		this.draw_loop();
	}

	/**
	 * Stop the rendering
	 */
	stop() {
		if (this.animation_frame_idx) {
			cancelAnimationFrame(this.animation_frame_idx);
		}
		this.is_running = false;
	}

	/**
	 * Draw a single step of the world. Useful to render the world before the simulation starts
	 */
	init() {
		this.draw();
	}

	/**
	 * The draw loop
	 */
	private draw_loop() {
		this.animation_frame_idx = requestAnimationFrame(() => this.draw_loop());
		this.draw();
	}

	/**
	 * Actually draws the world
	 */
	private draw() {
		this.fix_dpi();
		this.ctx.fillStyle = '#ffffff';
		this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.width);
		this.world.bodies.forEach((body: Body) => {
			body.render(this.ctx);
		});
	}
}
