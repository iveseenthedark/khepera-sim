import Vector from '../primitives/Vector';

export default class RenderUtils {
	/**
	 * Default implementation on body will draw a polygon created from the vertices
	 */
	static render_vertices(vertices: Vector[], ctx: CanvasRenderingContext2D) {
		ctx.beginPath();
		vertices.forEach((vertex: Vector) => {
			ctx.moveTo(vertex.a.x, vertex.a.y);
			ctx.lineTo(vertex.b.x, vertex.b.y);
		});
		ctx.stroke();
	}
}
