import Controller from './Controller';
import { Network } from 'neataptic';

export default class NeatapticController implements Controller {
	constructor(private readonly network: Network) {}

	update(inputs: number[]): number[] {
		return this.network.activate(inputs);
	}
}
