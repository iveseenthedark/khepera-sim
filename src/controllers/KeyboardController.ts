import Controller from './Controller';

export default class KeyboardController implements Controller {
	private static readonly VK = Object.freeze({
		LEFT: 37,
		RIGHT: 39,
		UP: 38,
		DOWN: 40
	});
	private readonly keys: boolean[];

	constructor() {
		this.keys = [];

		// Update key register
		document.body.addEventListener('keydown', e => {
			// noinspection JSDeprecatedSymbols
			this.keys[e.keyCode] = true;
		});
		document.body.addEventListener('keyup', e => {
			// noinspection JSDeprecatedSymbols
			this.keys[e.keyCode] = false;
		});
	}

	update(inputs: number[]): number[] {
		let activations = [0.5, 0.5];
		if (this.keys[KeyboardController.VK.UP]) {
			activations = [1, 1];
		}
		if (this.keys[KeyboardController.VK.LEFT]) {
			activations = [1, 0];
		}
		if (this.keys[KeyboardController.VK.RIGHT]) {
			activations = [0, 1];
		}
		return activations;
	}
}
