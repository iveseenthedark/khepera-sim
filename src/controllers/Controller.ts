export default interface Controller {
	update(inputs: number[]): number[];
}
