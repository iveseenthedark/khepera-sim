import './styles.scss';

import Renderer from './render/Renderer';
import World from './world/World';

import { StaticObstacle } from './world/StaticObstacle';
import Light from './world/Light';

import Agent from './world/Agent';
import PhotoSensor from './world/PhotoSensor';

import { architect, Network } from 'neataptic';
import NeatapticController from './controllers/NeatapticController';
import NeatapticWorker from 'worker-loader!./evolve/NeatapticWorker';
import Point from './primitives/Point';
import KeyboardController from "./controllers/KeyboardController";

// --------------------
// Setup World
// --------------------
const world = new World(640, 560);
world.add_element(
	...StaticObstacle.create_boundary_wall(world),
	new StaticObstacle([
		{ a: { x: 100, y: 250 }, b: { x: 120, y: 150 } },
		{ a: { x: 120, y: 150 }, b: { x: 200, y: 180 } },
		{ a: { x: 200, y: 180 }, b: { x: 140, y: 310 } },
		{ a: { x: 140, y: 310 }, b: { x: 100, y: 250 } }
	]),
	new StaticObstacle([
		{ a: { x: 100, y: 300 }, b: { x: 120, y: 350 } },
		{ a: { x: 120, y: 350 }, b: { x: 60, y: 400 } },
		{ a: { x: 60, y: 400 }, b: { x: 100, y: 300 } }
	]),
	new StaticObstacle([
		{ a: { x: 200, y: 360 }, b: { x: 220, y: 250 } },
		{ a: { x: 220, y: 250 }, b: { x: 300, y: 300 } },
		{ a: { x: 300, y: 300 }, b: { x: 350, y: 420 } },
		{ a: { x: 350, y: 420 }, b: { x: 200, y: 360 } }
	]),
	new StaticObstacle([
		{ a: { x: 340, y: 160 }, b: { x: 360, y: 140 } },
		{ a: { x: 360, y: 140 }, b: { x: 370, y: 170 } },
		{ a: { x: 370, y: 170 }, b: { x: 340, y: 160 } }
	]),
	new StaticObstacle([
		{ a: { x: 450, y: 290 }, b: { x: 560, y: 270 } },
		{ a: { x: 560, y: 270 }, b: { x: 540, y: 370 } },
		{ a: { x: 540, y: 370 }, b: { x: 430, y: 390 } },
		{ a: { x: 430, y: 390 }, b: { x: 450, y: 290 } }
	]),
	new StaticObstacle([
		{ a: { x: 400, y: 195 }, b: { x: 580, y: 150 } },
		{ a: { x: 580, y: 150 }, b: { x: 480, y: 250 } },
		{ a: { x: 480, y: 250 }, b: { x: 400, y: 195 } }
	])
);
world.add_element(new Light({ x: 310, y: 246 }));

// --------------------
// Create Brain
// --------------------
const controller = new KeyboardController();

// --------------------
// Create Agent
// --------------------
const agent = new Agent({ x: 19, y: 456 }, controller);
agent.add_sensor(new PhotoSensor(agent, Math.PI / 4));
agent.add_sensor(new PhotoSensor(agent, -Math.PI / 4));
world.add_element(agent);

// --------------------
// Start Simulation
// --------------------
window.addEventListener('load', () => {
	// Setup canvas
	const root: HTMLElement | null = document.querySelector('#canvas-wrapper');
	if (root) {
		const renderer = Renderer.create({
			element: root,
			world: world
		});
		setInterval(() => {
			world.update();
		}, 0);
		renderer.start();
	} else {
		console.log("Couldn't find the root element!");
	}
	// Listen for evolve presses
	const worker = new NeatapticWorker();

	// @ts-ignore
	document.querySelector('#start').addEventListener('click', () => {
		worker.postMessage({ cmd: 'start' });
	});
	// @ts-ignore
	document.querySelector('#stop').addEventListener('click', () => {
		worker.postMessage({ cmd: 'stop' });
	});

	worker.addEventListener('message', e => {
		const { fittest, score, iteration } = e.data;
		console.log(iteration, score, fittest);
		agent.controller = new NeatapticController(Network.fromJSON(fittest));
		agent.position = Point.random([10, world.width - 10], [10, world.height - 10]);
	});
});
