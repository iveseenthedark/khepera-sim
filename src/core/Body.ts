import World from '../world/World';
import Vector from '../primitives/Vector';
import RenderUtils from '../render/RenderUtils';
import Point from '../primitives/Point';
import SimMaths from '../maths/SimMaths';

/**
 */
export default abstract class Body {
	constructor(public vertices: Vector[] = []) {}

	abstract update(world: World): void;

	/**
	 * Default render will draw the vertices as a polygon
	 */
	render(ctx: CanvasRenderingContext2D): void {
		RenderUtils.render_vertices(this.vertices, ctx);

		const centre = this.centroid();
		ctx.beginPath();
		ctx.arc(centre.x, centre.y, 0.5, 0, Math.PI * 2);
		ctx.stroke();
	}

	/**
	 * Get all the points that are corners on this body
	 */
	get_points(): Point[] {
		return this.vertices.reduce((vps: Point[], vector: Vector) => {
			return [...vps, vector.a, vector.b];
		}, []);
	}

	/**
	 * Test is a point is contained within this body
	 */
	contains(point: Point): boolean {
		return SimMaths.segments_surround(this.vertices, point);
	}

	/**
	 * Return the centroid of this body
	 */
	centroid(): Point {
		return SimMaths.get_polygon_centroid(this.get_points());
	}

	/**
	 * Returns is the body has a solid form that can be collided with
	 */
	is_solid(): boolean {
		return true;
	}
}

/**
 */
export abstract class LightSource extends Body {
	constructor(public vertices: Vector[] = [], public luminance: number) {
		super(vertices);
	}
}
