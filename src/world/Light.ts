import { LightSource } from '../core/Body';
import Point from '../primitives/Point';
import Vector from '../primitives/Vector';
import World from './World';
import RenderUtils from '../render/RenderUtils';
import Intersection from '../primitives/Intersection';

export default class Light extends LightSource {
	constructor(public position: Point, public readonly luminance: number = 200) {
		super([], luminance);
	}

	move(point: Point) {
		this.position.x = point.x;
		this.position.y = point.y;
	}

	render(ctx: CanvasRenderingContext2D): void {
		RenderUtils.render_vertices(this.vertices, ctx);
		ctx.beginPath();
		ctx.arc(this.position.x, this.position.y, 5, 0, Math.PI * 2);
		ctx.stroke();
	}

	update(world: World): void {
		this.vertices.length = 0;
		// Get all the corners of the bodies in the world
		let points = world.unique_points();
		// Build list of angles between light and corners
		const unique_angles = points.reduce((angles: number[], point: Point) => {
			const angle = Math.atan2(point.y - this.position.y, point.x - this.position.x);
			// The two extra rays are needed to hit the wall(s) behind any given segment corner.
			return [...angles, angle - 0.00001, angle, angle + 0.00001];
		}, []);

		// Check the intersection to see if a corner if blocked by another body
		let intersections: Array<{ angle: number; intersection: Intersection }> = unique_angles.reduce(
			(intersections, angle) => {
				const dx = Math.cos(angle);
				const dy = Math.sin(angle);

				const ray: Vector = {
					a: this.position,
					b: { x: this.position.x + dx, y: this.position.y + dy }
				};
				const intersection = world.get_closest_intersection(ray);
				return intersection !== null ? [...intersections, { angle, intersection }] : intersections;
			},
			new Array<{ angle: number; intersection: Intersection }>()
		);

		// Convert to polygon
		intersections = intersections.sort((a, b) => a.angle - b.angle);
		let previousIntersect = intersections[0];
		for (let i = 1, intersection; (intersection = intersections[i++]); ) {
			this.vertices.push({
				a: { x: previousIntersect.intersection.x, y: previousIntersect.intersection.y },
				b: { x: intersection.intersection.x, y: intersection.intersection.y }
			});
			previousIntersect = intersection;
		}
	}

	is_solid(): boolean {
		return false;
	}
}
