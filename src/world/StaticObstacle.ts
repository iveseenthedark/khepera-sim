import World from './World';
import Vector from '../primitives/Vector';
import Body from '../core/Body';
import Point from '../primitives/Point';

/**
 */
export class StaticObstacle extends Body {
	constructor(public vertices: Array<Vector>) {
		super(vertices);
	}

	update(world: World): void {
		// no-op: static obstacles don't move!
	}

	/**
	 * Create a boundary wall for the given world
	 */
	static create_boundary_wall(world: World, depth = 3): StaticObstacle[] {
		return [
			/* Top    */ StaticObstacle.rectangle({ x: 0, y: 0 }, world.width, depth),
			/* Right  */ StaticObstacle.rectangle({ x: world.width - depth, y: 0 }, depth, world.height),
			/* Bottom */ StaticObstacle.rectangle({ x: 0, y: world.height - depth }, world.width, depth),
			/* Left   */ StaticObstacle.rectangle({ x: 0, y: 0 }, depth, world.height)
		];
	}

	/**
	 * Create a rectangular shaped static object
	 */
	static rectangle(top_left: Point, width: number, height: number) {
		const { x, y } = top_left;
		return new StaticObstacle([
			/* Top    */ { a: { x, y }, b: { x: x + width, y: y } },
			/* Right  */ { a: { x: x + width, y: y }, b: { x: x + width, y: y + height } },
			/* Bottom */ { a: { x: x + width, y: y + height }, b: { x: x, y: y + height } },
			/* Left   */ { a: { x: x, y: y + height }, b: { x, y } }
		]);
	}
}
