import Body, { LightSource } from '../core/Body';
import Point from '../primitives/Point';
import Vector from '../primitives/Vector';
import SimMaths from '../maths/SimMaths';
import Intersection from '../primitives/Intersection';
import * as GreinerHormann from 'greiner-hormann';
import Agent from './Agent';

/**
 */
export default class World {
	static readonly DT = 0.2;
	readonly bodies: Array<Body> = new Array<Body>();

	constructor(public width: number, public height: number, public DT: number = 0.2) {}

	/**
	 * Add a new element to the world
	 */
	add_element<T extends Body>(...elements: T[]) {
		this.bodies.push(...elements);
	}

	/**
	 * Run a single update on all elements in the world
	 */
	update() {
		this.bodies.forEach((ele: Body) => {
			ele.update(this);
		});
	}

	/**
	 * Return a list of all the corners on all the bodies
	 */
	unique_points(): Point[] {
		// Get unique points
		const points = this.bodies.reduce((acc: Point[], body: Body) => {
			const body_points = body.vertices.reduce((vps: Point[], vector: Vector) => {
				return [...vps, ...body.get_points()];
			}, []);
			return [...acc, ...body_points];
		}, []);

		return points.reduce((points: Point[], point: Point) => {
			const unique = points.some(p => p.x === point.x && p.y === point.y);
			return unique ? points : [...points, point];
		}, []);
	}

	/**
	 * Return the closest intersection of a ray
	 */
	get_closest_intersection(ray: Vector): Intersection | null {
		let closest: Intersection | null = null;
		this.bodies.forEach((body: Body) => {
			body.vertices.forEach((vertex: Vector) => {
				const intersection = SimMaths.get_intersection(ray, vertex);
				if (intersection !== null && (closest === null || intersection.distance < closest.distance)) {
					closest = intersection;
				}
			});
		});
		return closest;
	}

	/**
	 * Get the light brightness at a given point
	 */
	get_illuminance_at(point: Point): number {
		return this.bodies.reduce((illuminance: number, body: Body) => {
			if (body instanceof LightSource && body.contains(point)) {
				const intensity = (body.luminance - SimMaths.distance(body.centroid(), point)) / body.luminance;
				illuminance += intensity < 0 ? 0 : intensity;
			}
			return illuminance;
		}, 0);
	}

	/**
	 * Test whether given body is colliding with any other bodies
	 */
	query_collision(body: Body) {
		return this.bodies
			.filter(target => target.is_solid() && target !== body)
			.map(target => {
				return GreinerHormann.intersection(body.get_points(), target.get_points());
			})
			.filter(intersection => intersection != null);
	}
}
