import Body from '../core/Body';
import Point from '../primitives/Point';
import World from './World';
import Vector from '../primitives/Vector';
import PhotoSensor from './PhotoSensor';
import RenderUtils from '../render/RenderUtils';
import Controller from '../controllers/Controller';

export default class Agent extends Body {
	private sensors: PhotoSensor[] = [];
	public readonly radius: number = 15;

	constructor(public position: Point, public controller: Controller, public angle: number = 0) {
		super();
		this.update_vertices();
	}

	/** */
	add_sensor(sensor: PhotoSensor) {
		this.sensors.push(sensor);
	}

	/** */
	update(world: World): void {
		const activations = this.sensors.map(sensor => sensor.activation(world));
		const outputs = this.controller.update(activations);

		// Calc next location
		const vl = -5 + outputs[0] * 10;
		const vr = -5 + outputs[1] * 10;

		const vc = (vr + vl) / 2;
		const dv = (vr - vl) / (2 * this.radius);

		this.angle = (this.angle + dv * World.DT) % (Math.PI * 2);
		const x = this.position.x + Math.cos(this.angle) * (vc * World.DT);
		const y = this.position.y + Math.sin(this.angle) * (vc * World.DT);
		this.move_to({ x, y }, world);
	}

	/** */
	render(ctx: CanvasRenderingContext2D): void {
		RenderUtils.render_vertices(this.vertices, ctx);

		// Draw directions
		const { x, y } = this.position;
		ctx.beginPath();
		ctx.moveTo(x, y);
		ctx.lineTo(x + this.radius * Math.cos(this.angle), y + this.radius * Math.sin(this.angle));
		ctx.stroke();

		// Render sensors
		this.sensors.forEach(sensor => sensor.render(ctx));
	}

	/**
	 * Update the location of this agent. Checks if there are any collisions
	 */
	private move_to(point: Point, world: World) {
		// Cache current position in case the move leads to a collision
		const prev_point = this.position;

		// Move to new position
		this.position = point;
		this.update_vertices();

		// Query for collisions
		const collisions = world.query_collision(this);
		if (collisions.length) {
			// Reset point if we collided with something
			this.position = prev_point;
			this.update_vertices();
		}
	}

	/**
	 * Set the positions of the vertices
	 */
	private update_vertices() {
		this.vertices.length = 0;
		const vertex_inc = Math.PI / 4;
		let prev_point = {
			x: this.position.x + this.radius * Math.cos(0),
			y: this.position.y + this.radius * Math.sin(0)
		};
		for (let offset = vertex_inc; offset < 2 * Math.PI + vertex_inc; offset += vertex_inc) {
			const next_point = {
				x: this.position.x + this.radius * Math.cos(offset),
				y: this.position.y + this.radius * Math.sin(offset)
			};
			this.vertices.push(new Vector(prev_point, next_point));
			prev_point = next_point;
		}
	}
}
