import Agent from './Agent';
import Point from '../primitives/Point';
import World from './World';

export default class PhotoSensor {
	constructor(public readonly agent: Agent, private offset: number = 0) {}

	position(): Point {
		return new Point(
			this.agent.position.x + this.agent.radius * Math.cos(this.offset + this.agent.angle),
			this.agent.position.y + this.agent.radius * Math.sin(this.offset + this.agent.angle)
		);
	}

	render(ctx: CanvasRenderingContext2D): void {
		const position = this.position();
		ctx.beginPath();
		ctx.arc(position.x, position.y, 2, 0, Math.PI * 2);
		ctx.stroke();
	}

	activation(world: World): number {
		return world.get_illuminance_at(this.position());
	}
}
