import Point from './Point';

/**
 */
export default class Vector {
	constructor(public a: Point, public b: Point) {}
}
