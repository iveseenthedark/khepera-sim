/**
 */
import SimMaths from '../maths/SimMaths';

export default class Point {
	constructor(public x: number, public y: number) {}

	static random(x: [number, number], y: [number, number]) {
		return new Point(SimMaths.random(x), SimMaths.random(y));
	}
}
