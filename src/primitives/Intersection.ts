import Point from './Point';

export default class Intersection extends Point {
	constructor(x: number, y: number, public distance: number) {
		super(x, y);
	}
}
