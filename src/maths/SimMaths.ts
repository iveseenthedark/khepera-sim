import Vector from '../primitives/Vector';
import Point from '../primitives/Point';
import Intersection from '../primitives/Intersection';

/**
 * The majority of this code was stolen from https://ncase.me/sight-and-light/. It's an awesome article!
 */
export default class SimMaths {
	/**
	 * Does a polygon described by the set of segments surround the point
	 * ray-casting algorithm based on http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
	 */
	static segments_surround(segments: Vector[], { x, y }: Point): boolean {
		let inside: boolean = false;
		segments.forEach((seg: Vector) => {
			const xi = seg.a.x,
				yi = seg.a.y;
			const xj = seg.b.x,
				yj = seg.b.y;
			const intersect: boolean = yi > y != yj > y && x < ((xj - xi) * (y - yi)) / (yj - yi) + xi;
			if (intersect) inside = !inside;
		});

		return inside;
	}

	/**
	 * Stole this code from http://ncase.me/sight-and-light/
	 */
	static get_intersection(ray: Vector, vector: Vector): Intersection | null {
		// RAY in parametric: Point + Direction*T1
		const r_px = ray.a.x;
		const r_py = ray.a.y;
		const r_dx = ray.b.x - ray.a.x;
		const r_dy = ray.b.y - ray.a.y;

		// VECTOR in parametric: Point + Direction*T2
		const s_px = vector.a.x;
		const s_py = vector.a.y;
		const s_dx = vector.b.x - vector.a.x;
		const s_dy = vector.b.y - vector.a.y;

		// Are they parallel? If so, no intersect
		const r_mag = Math.sqrt(r_dx * r_dx + r_dy * r_dy);
		const s_mag = Math.sqrt(s_dx * s_dx + s_dy * s_dy);
		if (r_dx / r_mag == s_dx / s_mag && r_dy / r_mag == s_dy / s_mag) {
			// Directions are the same.
			return null;
		}

		// SOLVE FOR T1 & T2
		// r_px+r_dx*T1 = s_px+s_dx*T2 && r_py+r_dy*T1 = s_py+s_dy*T2
		// ==> T1 = (s_px+s_dx*T2-r_px)/r_dx = (s_py+s_dy*T2-r_py)/r_dy
		// ==> s_px*r_dy + s_dx*T2*r_dy - r_px*r_dy = s_py*r_dx + s_dy*T2*r_dx - r_py*r_dx
		// ==> T2 = (r_dx*(s_py-r_py) + r_dy*(r_px-s_px))/(s_dx*r_dy - s_dy*r_dx)
		const T2 = (r_dx * (s_py - r_py) + r_dy * (r_px - s_px)) / (s_dx * r_dy - s_dy * r_dx);
		const T1 = (s_px + s_dx * T2 - r_px) / r_dx;

		// Must be within parametic whatevers for RAY/SEGMENT
		if (T1 < 0) return null;
		if (T2 < 0 || T2 > 1) return null;

		// Return the POINT OF INTERSECTION
		return {
			x: r_px + r_dx * T1,
			y: r_py + r_dy * T1,
			distance: T1
		};
	}

	/**
	 * Returns the centroid of non-self-intersecting closed polygon
	 * https://en.wikipedia.org/wiki/Centroid#Of_a_polygon
	 */
	static get_polygon_centroid(points: Point[]): Point {
		const first = points[0];
		const last = points[points.length - 1];
		if (first.x != last.x || first.y != last.y) points.push(first);
		let twice_area = 0,
			x = 0,
			y = 0,
			f: number;
		for (let i = 0, j = points.length - 1; i < points.length; j = i++) {
			const p1 = points[i];
			const p2 = points[j];
			f = (p1.y - first.y) * (p2.x - first.x) - (p2.y - first.y) * (p1.x - first.x);
			twice_area += f;
			x += (p1.x + p2.x - 2 * first.x) * f;
			y += (p1.y + p2.y - 2 * first.y) * f;
		}
		f = twice_area * 3;
		return { x: x / f + first.x, y: y / f + first.y };
	}

	/**
	 * Distance between two points
	 */
	static distance(a: Point, b: Point) {
		return Math.sqrt(Math.pow(a.x - b.x, 2) + Math.pow(a.y - b.y, 2));
	}

	/**
	 * Returns a random integer between min (include) and max (include)
	 */
	static random(range: [number, number]) {
		return Math.floor(Math.random() * (range[1] - range[0] + 1)) + range[0];
	}
}
