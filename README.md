# Khepera Sim

A really simple Khepera Simulator for experimenting with evolutionary robotics.

## Building and running on localhost

First install dependencies:

```sh
yarn install
```

To run in the browser

```sh
yarn start
```

## Credits

Created by [iveseenthedark](https://iveseenthedark.bitbucket.io/)
